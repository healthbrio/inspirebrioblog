

/*angular.module('myApp', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider){
        $routeProvider.when('/symptoms', {templateUrl: 'templates/symptoms/symptoms.html'})
        .when('/conditions', {templateUrl: 'templates/conditions/conditions.html'})

    }]);*/



(function () {
  'use strict';

  var app=angular
    .module('myApp', [
      //'myApp.config',

      //'ngAnimate',
	  'myApp.routes',
      'myApp.primary',
      
    ]);

  angular.module('myApp.config', []);

  angular.module('myApp.routes', ['ngRoute', 'ui.router']).run(function($rootScope) {
    /*$rootScope.static_path = 'https://healthbrio.s3.amazonaws.com/';*/
    $rootScope.static_path = 'http://127.0.0.1:8000/static/';
  });

})();






